package com.example.tests;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class Buy {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://demo.litecart.net/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testBuy() throws Exception {
    driver.get(baseUrl + "/en/rubber-ducks-c-1/red-duck-p-3");
    String title = driver.findElement(By.xpath("//div[@id='box-product']/div/h1")).getText();
    System.out.println(title);
    driver.findElement(By.name("add_cart_product")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if ("Cart:\n 1 item(s) - $20".equals(driver.findElement(By.cssSelector("a.content")).getText())) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    driver.findElement(By.cssSelector("a.content")).click();
    for (int second = 0;; second++) {
    	if (second >= 60) fail("timeout");
    	try { if (title.equals(driver.findElement(By.xpath("//div[@id='box-checkout-cart']/div/ul/li/form/div/p/a/strong")).getText())) break; } catch (Exception e) {}
    	Thread.sleep(1000);
    }

    assertEquals(title, driver.findElement(By.xpath("//div[@id='box-checkout-cart']/div/ul/li/form/div/p/a/strong")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
